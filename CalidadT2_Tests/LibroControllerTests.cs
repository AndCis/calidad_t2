﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositorios;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2_Tests
{
    public class LibroControllerTests
    {
        [Test]
        public void SeMuestranDetalles()
        {
            var libMock = new Mock<ILibroRepositorio>();
            libMock.Setup(o => o.Detalles(1));

            var contLib = new LibroController(null, libMock.Object, null);
            var det = contLib.Details(1);

            Assert.IsInstanceOf<ViewResult>(det);
        }

        [Test]
        public void Usuario1AgregaComentario()
        {
            var usuario = new Usuario();
            usuario.Id = 1;
            usuario.Nombres = "Andres";
            usuario.Username = "Admin";
            usuario.Password = "Admin";

            var comentario = new Comentario();
            comentario.Id = 1;
            comentario.UsuarioId = 1;
            comentario.LibroId = 1;
            comentario.Texto = "Hola";
            comentario.Fecha = DateTime.Now;
            comentario.Puntaje = 2;
            comentario.Usuario = usuario;

            var userMock = new Mock<IUsuarioRepositorio>();
            userMock.Setup(o => o.ObtenerUsuario(null)).Returns(usuario);

            var cookMock = new Mock<ICokkieRepository>();
            var libMock = new Mock<ILibroRepositorio>();
            libMock.Setup(o => o.AgregarComentario(1, comentario));

            var contLib = new LibroController(cookMock.Object, libMock.Object, userMock.Object);
            var coment = contLib.AddComentario(comentario);

            Assert.IsInstanceOf<RedirectToActionResult>(coment);
        }
    }
}
