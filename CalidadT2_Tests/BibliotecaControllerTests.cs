﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositorios;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2_Tests
{
    public class BibliotecaControllerTests
    {
        [Test]
        public void UsuarioAgregaLibro3()
        {
            var biblioMock = new Mock<IBibliotecaRepositorio>();
            biblioMock.Setup(o => o.AgregarLibro(1, 2));

            var cookieMock = new Mock<ICokkieRepository>();

            var usuario = new Usuario();
            usuario.Id = 2;
            usuario.Nombres = "Andres";
            usuario.Username = "admin";
            usuario.Password = "admin";
            var userMock = new Mock<IUsuarioRepositorio>();
            userMock.Setup(o => o.ObtenerUsuario(null)).Returns(usuario);

            var biContr = new BibliotecaController(cookieMock.Object, biblioMock.Object, userMock.Object);
            var agreg = biContr.Add(1);
            Assert.IsInstanceOf<RedirectToActionResult>(agreg);
            
        }

        [Test]
        public void IndexCarga()
        {
            var usuario = new Usuario();
            usuario.Id = 1;
            usuario.Nombres = "Andres";
            usuario.Username = "admin";
            usuario.Password = "admin";
            var userMock = new Mock<IUsuarioRepositorio>();
            userMock.Setup(o => o.ObtenerUsuario(null)).Returns(usuario);

            var cookieMock = new Mock<ICokkieRepository>();

            var bibMock = new Mock<IBibliotecaRepositorio>();
            bibMock.Setup(o => o.ConsultaIndex(1));

            var bibCont = new BibliotecaController(cookieMock.Object, bibMock.Object, userMock.Object);
            var ind = bibCont.Index();

            Assert.IsInstanceOf<ViewResult>(ind);
                
        }

        [Test]
        public void Usuario1MarcaLibro2Leyendo()
        {
            var usuario = new Usuario();
            usuario.Id = 1;
            usuario.Nombres = "Andres";
            usuario.Username = "admin";
            usuario.Password = "admin";
            var userMock = new Mock<IUsuarioRepositorio>();
            userMock.Setup(o => o.ObtenerUsuario(null)).Returns(usuario);

            var cookieMock = new Mock<ICokkieRepository>();

            var bibMock = new Mock<IBibliotecaRepositorio>();
            bibMock.Setup(o => o.MarcarLeyendo(2, 1));

            var bibCont = new BibliotecaController(cookieMock.Object, bibMock.Object, userMock.Object);
            var ind = bibCont.MarcarComoLeyendo(2);

            Assert.IsInstanceOf<RedirectToActionResult>(ind);

        }

        [Test]
        public void Usuario1MarcaLibro2Terminado()
        {
            var usuario = new Usuario();
            usuario.Id = 1;
            usuario.Nombres = "Andres";
            usuario.Username = "admin";
            usuario.Password = "admin";
            var userMock = new Mock<IUsuarioRepositorio>();
            userMock.Setup(o => o.ObtenerUsuario(null)).Returns(usuario);

            var cookieMock = new Mock<ICokkieRepository>();

            var bibMock = new Mock<IBibliotecaRepositorio>();
            bibMock.Setup(o => o.MarcarTerminado(2, 1));

            var bibCont = new BibliotecaController(cookieMock.Object, bibMock.Object, userMock.Object);
            var ind = bibCont.MarcarComoTerminado(2);

            Assert.IsInstanceOf<RedirectToActionResult>(ind);

        }
    }
}
