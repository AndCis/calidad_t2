﻿using CalidadT2.Controllers;
using CalidadT2.Repositorios;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2_Tests
{
    public class HomeControllerTests
    {
        [Test]
        public void IndexFunciona()
        {
            var homMock = new Mock<IHomeRepositorio>();
            homMock.Setup(o => o.index());

            var cont = new HomeController(homMock.Object);
            var ind = cont.Index();
            Assert.IsInstanceOf<ViewResult>(ind);
        }
        

    }
}
