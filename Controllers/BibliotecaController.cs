﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Constantes;
using CalidadT2.Models;
using CalidadT2.Repositorios;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    [Authorize]
    public class BibliotecaController : Controller
    {
        private readonly ICokkieRepository _cookkie;
        private readonly IUsuarioRepositorio _user;
        private readonly IBibliotecaRepositorio _biblio;

        public BibliotecaController(ICokkieRepository _cookkie, IBibliotecaRepositorio _biblio, IUsuarioRepositorio _user)
        {
            this._cookkie = _cookkie;
            this._biblio = _biblio;
            this._user = _user;
        }

        [HttpGet]
        public IActionResult Index()
        {
            Usuario user = LoggedUser();

            var model = _biblio.ConsultaIndex(user.Id);

            return View(model);
        }

        [HttpGet]
        public ActionResult Add(int libro)
        {
            Usuario user = LoggedUser();

            _biblio.AgregarLibro(libro, user.Id);

            ModelState.AddModelError("SuccessMessage", "Se añádio el libro a su biblioteca");
            //TempData["SuccessMessage"] = "Se añádio el libro a su biblioteca";

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult MarcarComoLeyendo(int libroId)
        {
            Usuario user = LoggedUser();

            _biblio.MarcarLeyendo(libroId, user.Id);

            ModelState.AddModelError("SuccessMessage", "Se añádio el libro a su biblioteca");
            //TempData["SuccessMessage"] = "Se marco como leyendo el libro";

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult MarcarComoTerminado(int libroId)
        {
            Usuario user = LoggedUser();

            _biblio.MarcarTerminado(libroId, user.Id);

            ModelState.AddModelError("SuccessMessage", "Se marco como leyendo el libro");
            //TempData["SuccessMessage"] = "Se marco como leyendo el libro";

            return RedirectToAction("Index");
        }

        private Usuario LoggedUser()
        {
            _cookkie.SetHttpContext(HttpContext);
            var claim = _cookkie.obtenerClaim();
            var user = _user.ObtenerUsuario(claim);
            return user;
        }
    }
}
