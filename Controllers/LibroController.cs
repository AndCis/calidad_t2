﻿using System;
using System.Linq;
using CalidadT2.Models;
using CalidadT2.Repositorios;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    public class LibroController : Controller
    {
        private readonly ICokkieRepository _cookie;
        private readonly ILibroRepositorio _libro;
        private readonly IUsuarioRepositorio _user;

        public LibroController(ICokkieRepository _cookie, ILibroRepositorio _libro, IUsuarioRepositorio _user)
        {
            this._cookie = _cookie;
            this._libro = _libro;
            this._user = _user;
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var model = _libro.Detalles(id);
            return View(model);
        }

        [HttpPost]
        public IActionResult AddComentario(Comentario comentario)
        {
            Usuario user = LoggedUser();

            _libro.AgregarComentario(user.Id, comentario);

            return RedirectToAction("Details", new { id = comentario.LibroId });
        }

        private Usuario LoggedUser()
        {
            _cookie.SetHttpContext(HttpContext);
            var claim = _cookie.obtenerClaim();
            var user = _user.ObtenerUsuario(claim);
            return user;
        }
    }
}
