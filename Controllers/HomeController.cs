﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using CalidadT2.Repositorios;

namespace CalidadT2.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHomeRepositorio _home;
        public HomeController(IHomeRepositorio _home)
        {
            this._home = _home;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = _home.index();
            return View(model);
        }
    }
}
