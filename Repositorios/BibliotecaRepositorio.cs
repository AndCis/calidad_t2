﻿using CalidadT2.Constantes;
using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repositorios
{
    
    
    public interface IBibliotecaRepositorio
    {
        List<Biblioteca> ConsultaIndex(int idUser);
        void AgregarLibro(int idLibro, int idUser);
        void MarcarLeyendo(int idLibro, int idUser);
        void MarcarTerminado(int idLibro, int idUser);

    }

    public class BibliotecaRepositorio : IBibliotecaRepositorio
    {
        private readonly AppBibliotecaContext _context;

        public BibliotecaRepositorio(AppBibliotecaContext _context)
        {
            this._context = _context;
        }

        public void AgregarLibro(int idLibro, int idUser)
        {
            var biblioteca = new Biblioteca
            {
                LibroId = idLibro,
                UsuarioId = idUser,
                Estado = ESTADO.POR_LEER
            };

            _context.Bibliotecas.Add(biblioteca);
            _context.SaveChanges();

            
        }

        public List<Biblioteca> ConsultaIndex(int idUser)
        {           

            var lista = _context.Bibliotecas
                .Include(o => o.Libro.Autor)
                .Include(o => o.Usuario)
                .Where(o => o.UsuarioId == idUser)
                .ToList();

            return lista;
        }

        public void MarcarLeyendo(int idLibro, int idUser)
        {
            var libro = _context.Bibliotecas
                .Where(o => o.LibroId == idLibro && o.UsuarioId == idUser)
                .FirstOrDefault();

            libro.Estado = ESTADO.LEYENDO;
            _context.SaveChanges();
        }

        public void MarcarTerminado(int idLibro, int idUser)
        {
            var libro = _context.Bibliotecas
                .Where(o => o.LibroId == idLibro && o.UsuarioId == idUser)
                .FirstOrDefault();

            libro.Estado = ESTADO.TERMINADO;
            _context.SaveChanges();
        }

        
    }
}
