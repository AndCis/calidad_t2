﻿using CalidadT2.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CalidadT2.Repositorios
{
    public interface IUsuarioRepositorio
    {
        Usuario ObtenerUsuario(Claim claim);
    }

    public class UsuarioRepositorio : IUsuarioRepositorio
    {
       

        private readonly AppBibliotecaContext _context;

        public UsuarioRepositorio(AppBibliotecaContext _context)
        {
            this._context = _context;
        }
        public Usuario ObtenerUsuario(Claim claim)
        {
            var user = _context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            return user;
        }
    }
}
