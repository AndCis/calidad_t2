﻿using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repositorios
{
    public interface IHomeRepositorio
    {
        List<Libro> index();
    }

    public class HomeRepositorio : IHomeRepositorio
    {
        private readonly AppBibliotecaContext _context;

        public HomeRepositorio(AppBibliotecaContext _context)
        {
            this._context = _context;
        }
        public List<Libro> index()
        {
            var model = _context.Libros.Include(o => o.Autor).ToList();
            return model;
        }
    }
}
