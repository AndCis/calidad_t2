﻿using CalidadT2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repositorios
{
    public interface IAuthRepository
    {
        Usuario UserLogueado(string username, string password);
    }

    public class AuthRepository : IAuthRepository
    {
        private readonly AppBibliotecaContext _context;

        public AuthRepository(AppBibliotecaContext _context)
        {
            this._context = _context;
        }

        public Usuario UserLogueado(string username, string password)
        {
            var usuario = _context.Usuarios.Where(o => o.Username == username && o.Password == password).FirstOrDefault();
            return usuario;
        }
    }
}
