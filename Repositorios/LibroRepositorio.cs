﻿using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repositorios
{
    public interface ILibroRepositorio
    {
        Libro Detalles(int id);
        void AgregarComentario(int idUser, Comentario comentario);
    }

    public class LibroRepositorio : ILibroRepositorio
    {
        private readonly AppBibliotecaContext _context;

        public LibroRepositorio(AppBibliotecaContext _context)
        {
            this._context = _context;
        }

        public void AgregarComentario(int idUser, Comentario comentario)
        {
            comentario.UsuarioId = idUser;
            comentario.Fecha = DateTime.Now;
            _context.Comentarios.Add(comentario);

            var libro = _context.Libros.Where(o => o.Id == comentario.LibroId).FirstOrDefault();
            libro.Puntaje = (libro.Puntaje + comentario.Puntaje) / 2;
            _context.SaveChanges();
        }

        public Libro Detalles(int id)
        {
            var model = _context.Libros
                .Include("Autor")
                .Include("Comentarios.Usuario")
                .Where(o => o.Id == id)
                .FirstOrDefault();

            return model;
        }
    }
}
